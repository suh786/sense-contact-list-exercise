import { IContact } from './IContact';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  private _contacts: IContact[];
  private _contactsSubject: BehaviorSubject<IContact[]>;

  constructor() {
    this._contacts = [
      {
        name: 'alyssa',
        email: 'alyssa@c.com',
        tel: '999999999',
        notes: 'A note',
      },
      { name: 'ben', email: 'ben@c.com', tel: '888888', notes: 'A note' },
      { name: 'john', email: 'ben@c.com', tel: '777777', notes: 'A note' },
    ];

    this._contactsSubject = new BehaviorSubject<IContact[]>([]);
    this._contactsSubject.next(this._contacts);
  }
  public getConacts(): Observable<IContact[]> {
    return this._contactsSubject.asObservable();
  }

  public removeContact(contact: IContact) {
    const index = this._contacts.indexOf(contact);
    this._contacts.splice(index, 1);
    this._contactsSubject.next(this._contacts);
  }
}

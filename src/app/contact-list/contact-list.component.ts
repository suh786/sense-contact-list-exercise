import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IContact } from '../IContact';
import { Observable } from 'rxjs';
import { ContactService } from '../contacts.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css'],
})
export class ContactListComponent implements OnInit {
  constructor(private contactService: ContactService) {}

  @Input()
  contacts: Observable<IContact[]>;

  @Output()
  contactSelected: EventEmitter<IContact> = new EventEmitter();

  ngOnInit(): void {}

  onContactSelected(contact: IContact) {
    this.contactSelected.emit(contact);
  }

  requestDeleteContact(contact: IContact) {
    this.contactService.removeContact(contact);
  }
}

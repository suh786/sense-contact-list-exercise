import { Component, OnInit } from '@angular/core';
import { IContact } from './IContact';
import { ContactService } from './contacts.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  constructor(private contactService: ContactService) {}

  public contacts$: Observable<IContact[]>;

  ngOnInit(): void {
    this.contactService.getConacts().subscribe((contacts: IContact[]) => {
      if (this.selectedContact && !contacts.includes(this.selectedContact)) {
        this.selectedContact = null;
      }
    });
    this.contacts$ = this.contactService.getConacts();
  }

  selectedContact: IContact;

  onContactSelected(contact: IContact) {
    this.selectedContact = contact;
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { IContact } from '../IContact';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.css'],
})
export class ContactDetailComponent implements OnInit {
  constructor() {}

  @Input()
  contact: IContact;

  ngOnInit(): void {}
}

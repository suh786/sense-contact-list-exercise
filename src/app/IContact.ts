export interface IContact {
  name: string;
  tel: string;
  email: string;
  notes: string;
}

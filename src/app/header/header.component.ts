import { Component, Input, OnInit } from '@angular/core';
import { IContact } from '../IContact';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor() {}

  @Input()
  selectedContact: IContact;

  ngOnInit(): void {}
}
